## some changes needed to make the multi-juicer CTF thing play nice.

clone this repo
clone the multi-juicer repo: https://github.com/juice-shop/multi-juicer

```bash
# from inside the cloned multi-juicer repo 
helm install multi-juicer helm/multi-juicer/ -f helm/multi-juicer/values.yaml -f ../okd4-codeplus-multi-juicer/values.yaml
```